# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:49:19 2020

@author: NeerajVerma
"""

import pandas as pd
import numpy as np
#from pandas import datetime
from datetime import date, timedelta

from matplotlib import pyplot as plt
from pandas.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot
#from sklearn.metrics import mean_squared_error
#from sklearn import metrics

nday = 15
#year = 2020
def mean_absolute_percentage_error(y_true, y_pred): 
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


#index_col=0,
series_all = pd.read_excel("PBI_ORDER_SUMM_COUNT.xlsx",squeeze=True)
series_all.head()
series_all.tail()
series_all = series_all [['Date','Value']]
series_all['Weeknum'] = series_all['Date'].dt.dayofweek
series_all.tail(15)

series_wk = series_all.loc[series_all['Weeknum'] == 6]
series_all = series_all.loc[series_all['Weeknum'] != 6]
series_all.tail() 
series_wk.tail()

#Mpesa_File = pd.ExcelFile('Mpesa Sunrise.xlsx')
#series_all = pd.read_excel(Mpesa_File, 'TransactionVolume',squeeze=True)
#series_all = series_all.loc[series_all['TransactionType'] == 'Transfer']

series_all =series_all.sort_values(by=['Date'])
series_wk =series_wk.sort_values(by=['Date'])
#series_all  = series_all.set_index('Date') 

#series_all = pd.read_excel('Mpesa Sunrise.xlsx', header=0,  squeeze=True)

#series .columns

series_all = series_all [['Date', 'Value']]
series_all.rename(columns={'Date' :'DATE'},inplace = True)  
#series_all.rename(columns={'Value' :'Value'},inplace = True)  
series_all['Value'] = series_all['Value'].astype(np.float64)

series = series_all [:-13]
#series = series_all
series = series.set_index('DATE') 
series.plot()
pyplot.show()

autocorrelation_plot(series)
pyplot.show()

#series.to_excel('Transfer1.xlsx')
X =   series.values

size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]
history = [x for x in train]
predictions = list()
for t in range(len(test)):
#   print(t)
	model1 = ARIMA(history, order=(5,1,0))
	model_fit = model1.fit(disp=0)
	output = model_fit.forecast()
	yhat = output[0]
	predictions.append(yhat)
	obs = test[t]
	history.append(obs)
	print('predicted=%f, expected=%f' % (yhat, obs))
    


MAPE = mean_absolute_percentage_error(test, predictions)
print('Series 1 MAPE :  %.3f' % MAPE)

pyplot.plot(test)
pyplot.plot(predictions, color='red')
pyplot.show()


nday = 25
forecast, stderr, conf = model_fit.forecast(steps=nday)
#forecasts = model_fit.forecast(steps=nday)
#forecast = forecasts[0]
#conf[0]
#lowerlimit = conf[]
#upperlimit = conf[0][1]

dfl = pd.DataFrame(conf)
#dfl[0]
#series5.columns
series = pd.DataFrame(series.reset_index())
series.columns
maxdate = series['DATE'].max()
startdate = maxdate + timedelta(days=1)
#startdate = maxdate
enddate = maxdate + timedelta(days=nday)

dayseries = pd.date_range(startdate,enddate)
dayseries = pd.date_range(startdate,enddate).to_series()
dayseries = dayseries.dt.dayofweek 
dayseries = pd.DataFrame(dayseries) 
dayseries = dayseries .loc[dayseries[0] != 6 ]

dayseries = pd.DataFrame(dayseries)
dayseries = pd.DataFrame(dayseries.reset_index())

Actualvol = series_all.loc[series_all['DATE'].isin(dayseries['index']),'Value']


MAPEfinal = mean_absolute_percentage_error(Actualvol, forecast)
print('Series  MAPE : %.3f' % MAPEfinal)



df = pd.DataFrame( {'DATE': dayseries['index'],'Forecast_Volume': forecast , 'lowerlimit':dfl[0] , 'upperlimit': dfl[1] })
series.tail()
df.head(10)
df.to_excel("predectvalue.xlsx")

#df_final = series.append(df)
df_final = pd.merge(series_all,df,  on= ['DATE'],how = 'left')
#df_final = series_all.append(df)
df_final.tail(10) 
df_final.columns


df_final = df_final[['DATE','Value','Forecast_Volume','lowerlimit', 'upperlimit']]

df_final.tail(23)


##########################################
nday_w = 3
series_wk = series_wk [['Date','Value']]
series_wk.rename(columns={'Date' :'DATE'},inplace = True)  
series_w =series_wk.sort_values(by=['DATE'])

series_w = series_w [['DATE', 'Value']]
#series_w.rename(columns={'Date' :'DATE'},inplace = True)  

series_w['Value'] = series_w['Value'].astype(np.float64)

series_w = series_w [:-nday_w]

series_w = series_w.set_index('DATE') 
series_w.plot()
pyplot.show()

autocorrelation_plot(series_w)
pyplot.show()

X1 =   series_w.values

size = int(len(X1) * 0.66)
train_w, test_w = X1[0:size], X1[size:len(X1)]
history_w = [x for x in train_w]
predictions_w = list()
for t in range(len(test_w)):
#   print(t)
	model_w = ARIMA(history_w, order=(5,1,0))
	model_fit_w = model_w.fit(disp=0)
	output = model_fit_w.forecast()
	yhat = output[0]
	predictions_w.append(yhat)
	obs = test_w[t]
	history_w.append(obs)
	print('predicted=%f, expected=%f' % (yhat, obs))

MAPE_w = mean_absolute_percentage_error(test_w, predictions_w)
print('Series 1 MAPE :  %.3f' % MAPE_w)

pyplot.plot(test_w)
pyplot.plot(predictions_w, color='red')
pyplot.show()


#nday_w = 2
forecast_w, stderr, conf = model_fit_w.forecast(steps=nday_w)

dfl_w = pd.DataFrame(conf)

series_w = pd.DataFrame(series_w.reset_index())
series_w.columns
maxdate = series_w['DATE'].max()
startdate = maxdate + timedelta(days=7) 
#startdate = maxdate
enddate = startdate + timedelta(days=7)
#dayseries = pd.date_range(startdate,enddate)
dayseries_w = pd.date_range(startdate,enddate).to_series()
dayseries_w = dayseries_w.dt.dayofweek 
dayseries = pd.DataFrame(dayseries_w) 
dayseries_w = dayseries_w.loc[dayseries[0] == 6 ]

dayseries_w = pd.DataFrame(dayseries_w)
dayseries_w = pd.DataFrame(dayseries_w.reset_index())

Actualvol_w = series_wk.loc[series_wk['DATE'].isin(dayseries_w['index']),'Value']

MAPEfinal = mean_absolute_percentage_error(Actualvol, forecast)
print('Series  MAPE : %.3f' % MAPEfinal)

df_w = pd.DataFrame( {'DATE': dayseries_w['index'],'Forecast_Volume': forecast_w , 'lowerlimit':dfl_w[0] , 'upperlimit': dfl_w[1] })
series_w.tail()
df_w.head(10)
#df_final_w = series_w.append(df_w)
df_final_w = pd.merge(series_wk,df_w,  on= ['DATE'],how = 'left')
df_final_w.tail(10) 
df_final_w.columns
df_final_w = df_final_w[['DATE','Value','Forecast_Volume','lowerlimit', 'upperlimit']]

##########################################
df_final.sort_values(by=['DATE'])
df_final_all =  df_final.append(df_final_w)
df_final_all =df_final_all.sort_values(by=['DATE'])
df_final_all.to_excel('Order_Summ_forecast1.xlsx' ,index = False )
#df.to_excel('Volume_forecasted4.xlsx' ,index = False)
