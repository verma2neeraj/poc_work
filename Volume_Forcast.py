# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:49:19 2020

@author: NeerajVerma
"""

import pandas as pd
import numpy as np
#from pandas import datetime
from datetime import date, timedelta

from matplotlib import pyplot as plt
from pandas.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot
#from sklearn.metrics import mean_squared_error
#from sklearn import metrics

nday = 30
#year = 2020
def mean_absolute_percentage_error(y_true, y_pred): 
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


#index_col=0,
Mpesa_File = pd.ExcelFile('Mpesa Sunrise.xlsx')
series_all = pd.read_excel(Mpesa_File, 'TransactionVolume',squeeze=True)
series_all = series_all.loc[series_all['TransactionType'] == 'Transfer']
series_all =series_all.sort_values(by=['Date'])
#series_all = pd.read_excel('Mpesa Sunrise.xlsx', header=0,  squeeze=True)

#series .columns

series_all = series_all [['Date', 'Value']]
series_all.rename(columns={'Date' :'DATE'},inplace = True)  
series_all.rename(columns={'Value' :'Transfer'},inplace = True)  
series_all['Transfer'] = series_all['Transfer'].astype(np.float64)

series = series_all [:-6]
#series = series_all
series = series.set_index('DATE') 
series.plot()
pyplot.show()

autocorrelation_plot(series)
pyplot.show()

y = series['Transfer'].resample('W').sum()

import statsmodels.api as sm
decomposition = sm.tsa.seasonal_decompose(y, model='additive')
#decomposition = sm.tsa.seasonal_decompose(y, model='multiplicative')
decomposition.plot()
#
#decomposition.resid
#decomposition.seasonal
#decomposition.trend
#decomposition.observed

#series.to_excel('Transfer1.xlsx')
X =   series.values

size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]
history = [x for x in train]
predictions = list()
for t in range(len(test)):
#   print(t)
	model1 = ARIMA(history, order=(5,1,0))
	model_fit = model1.fit(disp=0)
	output = model_fit.forecast()
	yhat = output[0]
	predictions.append(yhat)
	obs = test[t]
	history.append(obs)
#	print('predicted=%f, expected=%f' % (yhat, obs))
    


MAPE = mean_absolute_percentage_error(test, predictions)
print('Series 1 MAPE :  %.3f' % MAPE)

pyplot.plot(test)
pyplot.plot(predictions, color='red')
pyplot.show()

#nday = 35
#nday = 7
forecast, stderr, conf = model_fit.forecast(steps=nday)
#forecasts = model_fit.forecast(steps=nday)
#forecast = forecasts[0]
#conf[0]
#lowerlimit = conf[]
#upperlimit = conf[0][1]

dfl = pd.DataFrame(conf)
#dfl[0]
#series5.columns
series = pd.DataFrame(series.reset_index())
series.columns
maxdate = series['DATE'].max()
startdate = maxdate + timedelta(days=1)
#startdate = maxdate
enddate = maxdate + timedelta(days=nday)

dayseries = pd.date_range(startdate,enddate)
dayseries
#ActualTPS = series_all.loc[series_all['DATE'].isin(dayseries),'Transfer']


MAPEfinal = mean_absolute_percentage_error(ActualTPS, forecast)
print('Series  MAPE : %.3f' % MAPEfinal)



df = pd.DataFrame( {'DATE': dayseries,'Forecast_Volume': forecast , 'lowerlimit':dfl[0] , 'upperlimit': dfl[1] })
series.tail()
df.tail(10)

#df_final = series.append(df)
df_final = pd.merge(series_all,df,  on= ['DATE'],how = 'left')
df_final = series_all.append(df)
df_final.tail() 
df_final.columns


df_final = df_final[['DATE','Transfer','Forecast_Volume','lowerlimit', 'upperlimit']]

df_final.tail(23)
df_final.to_excel('Volume_forecasted3.xlsx' ,index = False)
df.to_excel('Volume_forecasted4.xlsx' ,index = False)
