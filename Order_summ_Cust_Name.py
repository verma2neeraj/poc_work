# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 05:53:21 2020

@author: NeerajVerma
"""

import pandas as pd
import numpy as np
#from pandas import datetime
from datetime import date, timedelta

from matplotlib import pyplot as plt
from pandas.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot

def mean_absolute_percentage_error(y_true, y_pred): 
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


pweek = 17
nweek = pweek + 5

#Order_summ_File = pd.ExcelFile('Order_Summary_29_Jul.xlsx')
Order_summ_File = pd.ExcelFile('Order_summ_2_Aug_20.xlsx')
#series_all = pd.read_excel(Order_summ_File, 'Export Worksheet',squeeze=True)
series_all = pd.read_excel(Order_summ_File, 'Sheet1',squeeze=True)

df_cust_forcast = None
df_MAPE_Final = None
df_Actual_MAPE = None
#Cust_Name = 'LINK UP SERVICES LIMITED'


#df_pivot = pd.pivot_table(series_all ,index = ['Customer_Name'] , values = ['DISTINCT_ORDER_CNT'],aggfunc= {'DISTINCT_ORDER_CNT': [np.sum]} )
#df_pivot = pd.DataFrame (df_pivot.reset_index() )
#series_all['REQ_DATE'].dt.month.max() - 1

#group by for monthly order count as per the previous month 
#series = series_all.loc[(series_all['REQ_DATE'].dt.month == series_all['REQ_DATE'].dt.month.max() - 1) & (series_all['REQ_DATE'].dt.year == 2020)]
series_all['WeekNumber'] = (series_all['REQ_DATE'].map(lambda x:x.strftime('%V'))).astype(np.int64)
series_all['Week_Year']  = series_all['WeekNumber'].astype(np.str) + "-" + series_all['REQ_DATE'].map(lambda x:x.strftime('%Y'))

series = series_all.loc[(series_all['Month-Year'] == '6-2020')]

#series = series_all.loc[(series_all['Week_Year'] == '29-2020')]
df_pivot  = series.groupby(['Customer_Name'])['DISTINCT_ORDER_CNT'].aggregate('sum')
# group by for all the order count 
#df_pivot  = series_all.groupby(['Customer_Name'])['DISTINCT_ORDER_CNT'].aggregate('sum')
df_pivot = pd.DataFrame (df_pivot.reset_index() )
df_pivot = df_pivot.sort_values(by=['DISTINCT_ORDER_CNT'], ascending = False)
    
df_pivot = df_pivot.head(20) 
#df_pivot.loc[df_pivot['Customer_Name'] == Cust_Name]
ct = 'LINK UP SERVICES LIMITED'
#ct = 'Simba Telecom'
for ct in df_pivot['Customer_Name']:
    print(ct)
    series_CN = series_all.loc[series_all['Customer_Name'] == ct ]
    series_CN =series_CN.sort_values(by=['REQ_DATE'])
    
    series_CN.rename(columns={'REQ_DATE' :'DATE'},inplace = True)  
    
    
    series_CN.rename(columns={'DISTINCT_ORDER_CNT' :'ORDER_CNT'},inplace = True)  
    series_CN = series_CN [['DATE', 'ORDER_CNT']]
    
    series_CN['ORDER_CNT'] = series_CN['ORDER_CNT'].astype(np.float64)
    series_CN = series_CN.set_index('DATE') 
    
#    series_CN .plot()
#    pyplot.show()
#    
#    autocorrelation_plot(series_CN )
#    pyplot.show()
#    
    
    series_CN_wk_a = series_CN['ORDER_CNT'].resample('W').sum()
    series_CN_wk = series_CN_wk_a[:-pweek]
    #
#    import statsmodels.api as sm
#    decomposition = sm.tsa.seasonal_decompose(series_CN_wk, model='additive')
##    decomposition = sm.tsa.seasonal_decompose(series_CN_wk, model='multiplicative')
#    decomposition.plot()
#    
    
    X =   series_CN_wk.values
    
    size = int(len(X) * 0.66)
    train, test = X[0:size], X[size:len(X)]
    history = [x for x in train]
    predictions = list()
    for t in range(len(test)):
    #   print(t)
    	model = ARIMA(history, order=(2,1,0))
    	model_fit = model.fit(disp=0)
    	output = model_fit.forecast()
    	yhat = output[0]
    	predictions.append(yhat)
    	obs = test[t]
    	history.append(obs)
    	print('predicted=%f, expected=%f' % (yhat, obs))
        
    MAPE = mean_absolute_percentage_error(test, predictions)
    
    print(ct , '  MAPE :  %.3f' % MAPE)
    
    data = {'Cust_Name': [ct], 'MAPE_Val': [MAPE] }
    
    df_MAPE = pd.DataFrame( data)
    
    if isinstance(df_MAPE_Final, type(None)):
        df_MAPE_Final = df_MAPE
        print ('first time')
    else:
        df_MAPE_Final = df_MAPE_Final.append(df_MAPE) 
#    pyplot.plot(test)
#    pyplot.plot(predictions, color='red')
#    pyplot.show()
#    
    forecast, stderr, conf = model_fit.forecast(steps=nweek)
    
    dfl = pd.DataFrame(conf)
    #dfl[0]
    #series5.columns
    series_wk = pd.DataFrame(series_CN_wk.reset_index())
    #series.columns
    maxdate = series_wk['DATE'].max()
    
    startdate = maxdate + timedelta(days=7)
    enddate = startdate + timedelta(days=7*nweek-7)
    
    dayseries = pd.date_range(startdate,enddate,freq = 'W')
    
    
    df = pd.DataFrame( {'DATE': dayseries,'Forecast_Order_CNT': forecast , 'lowerlimit':dfl[0] , 'upperlimit': dfl[1], 'Cust_Name' : ct })
    #series_wk.tail()
    #df.head()
    
    series_CN_wk_a = pd.DataFrame(series_CN_wk_a.reset_index())
    df_actual = series_CN_wk_a.tail(pweek)
    
    df = pd.merge(df,df_actual,on  = ['DATE'],how = 'left')
    
    series_wk['Cust_Name'] = ct
    
    #df_final = pd.merge(series_wk,df,  on= ['DATE'],how = 'left')
    df_final = series_wk.append(df)
    
    df_final = df_final [['DATE', 'Cust_Name',  'ORDER_CNT', 'Forecast_Order_CNT','lowerlimit', 'upperlimit']]
    
    if isinstance(df_cust_forcast, type(None)):
        df_cust_forcast = df_final
        print ('yes')
    else:
        df_cust_forcast = df_cust_forcast.append(df_final) 
#    df_cust_forcast  = pd.merge(df_cust_forcast,df_final,on  = ['DATE'])
    
##    to get the Actual MAPE 
    df_actual = df_actual.set_index('DATE') 
    forecast1 = forecast[0:pweek]
    
#    df_actual = pd.DataFrame(df_actual.reset_index())
#    df_actual1 = df_actual.loc[df_actual['DATE'].isin(dayseries),'ORDER_CNT']
    
    MAPEfinal = mean_absolute_percentage_error(df_actual, forecast1)
    print('Series  MAPE : %.3f' % MAPEfinal)
    
    data_MAPE = {'Cust_Name': [ct], 'Final_MAPE': [MAPEfinal] }
    
    df_Final_MAPE = pd.DataFrame( data_MAPE)
    
    if isinstance(df_Actual_MAPE, type(None)):
        df_Actual_MAPE = df_Final_MAPE
        print ('first time')
    else:
        df_Actual_MAPE = df_Actual_MAPE.append(df_Final_MAPE)

#SAGE TELECOMMUNICATION LIMITED

df_cust_forcast.to_excel('Order_CNT_Forecast_1.xlsx' ,index = False)
df_MAPE_Final.to_excel('Order_MAPE_Result_1.xlsx' ,index = False)
df_Actual_MAPE.to_excel('Order_Final_MAPE_Result_1.xlsx' ,index = False)
#
#df_final.to_excel('Order_CNT_Forecast.xlsx' ,index = False)