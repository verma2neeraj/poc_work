# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:49:19 2020

@author: NeerajVerma
"""

import pandas as pd
import numpy as np
#from pandas import datetime
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from matplotlib import pyplot as plt
from pandas.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot
#from sklearn.metrics import mean_squared_error
#from sklearn import metrics

pmth = 4
nmth = pmth + 6
#year = 2020
def mean_absolute_percentage_error(y_true, y_pred): 
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


#index_col=0,
#series_all = pd.read_excel('CPUTPSData_Masterfile.xlsx', header=0,  squeeze=True)
series_all = pd.read_excel('TPS_DataCollection.xlsx', header=0,  squeeze=True)

series_all.columns

series_all = series_all [['DATE', 'PEAK_TPS']]
#series_all = series_all [['DATE', 'PEAK_TPS']]
series = series_all [:-pmth]
series = series_all 


#series = series_all
series = series.set_index('DATE') 

series = series['PEAK_TPS'].resample('M').max()
series.count()

series.plot()
pyplot.show()

autocorrelation_plot(series)
pyplot.show()


#y = series['Transfer'].resample('W').sum()

series_tr = series 
series_tr = series_tr.tail(42)
series_tr = series_tr.head(36)
import statsmodels.api as sm
decomposition = sm.tsa.seasonal_decompose(series_tr, model='additive')
#decomposition = sm.tsa.seasonal_decompose(series_tr, model='multiplicative')
decomposition.plot()
#
#decomposition.resid
decomposition.seasonal
#decomposition.trend
#decomposition.observed




X = series.values
size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]
history = [x for x in train]
predictions = list()
for t in range(len(test)):
#    print(t)
	model = ARIMA(history, order=(2,1,0))
	model_fit = model.fit(disp=0)
	output = model_fit.forecast()
	yhat = output[0]
	predictions.append(yhat)
	obs = test[t]
	history.append(obs)
	print('predicted=%f, expected=%f' % (yhat, obs))
    


MAPE = mean_absolute_percentage_error(test, predictions)
print('Series 1 MAPE :  %.3f' % MAPE)

pyplot.plot(test)
pyplot.plot(predictions, color='red')
pyplot.show()


#nday = 7
forecast, stderr, conf = model_fit.forecast(steps=nmth)
#forecasts = model_fit.forecast(steps=nday)
#forecast = forecasts[0]
#conf[0]
#lowerlimit = conf[]
#upperlimit = conf[0][1]

dfl = pd.DataFrame(conf)
#dfl[0]
#series5.columns
series = pd.DataFrame(series.reset_index())
series.columns
maxdate = series['DATE'].max()
startdate = maxdate + timedelta(days=30)
#startdate = maxdate
#enddate = maxdate + timedelta(days = 10)
enddate = maxdate + relativedelta(months = nmth )
dayseries = pd.date_range(startdate,enddate, freq = 'M')

ActualTPS = series_all.loc[series_all['DATE'].isin(dayseries),'PEAK_TPS']


MAPEfinal = mean_absolute_percentage_error(ActualTPS, forecast[0:pmth])
print('Series  MAPE : %.3f' % MAPEfinal)



df = pd.DataFrame( {'DATE': dayseries,'Forecast_PEAK_TPS': forecast , 'lowerlimit':dfl[0] , 'upperlimit': dfl[1] })

series.tail()
df.head()


df_final = pd.merge(series_all,df,  on= ['DATE'],how = 'left')
#df_final = series_all.append(df)

df_final.columns
df_final = df_final[['DATE','PEAK_TPS','Forecast_PEAK_TPS','lowerlimit', 'upperlimit']]


df1 = df[pmth:]

df_final = df_final.append(df1)

df_final = df_final[['DATE','PEAK_TPS','Forecast_PEAK_TPS','lowerlimit', 'upperlimit']]

df_final.tail()
df_final.to_excel('Mothly_PEAK_TPS_forecasted.xlsx' ,index = False)
#df.to_excel('TPS_forecasted2.xlsx' ,index = False)

