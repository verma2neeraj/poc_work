# -*- coding: utf-8 -*-
"""
https://ariepratama.github.io/How-to-do-conjoint-analysis-in-python/
Created on Sat Sep 12 19:47:04 2020

@author: NeerajVerma
"""

import pandas as pd
import numpy as np
# taken from imigrant conjoint data
df = pd.read_csv('https://dataverse.harvard.edu/api/access/datafile/2445996?format=tab&gbrecs=true', delimiter='\t')
df.head()

# checking for empty data
df.isnull().sum()

# remove empty data
clean_df = df[~df.rating.isnull()]

clean_df.isnull().sum() 

clean_df.head()

y = clean_df['selected']

x = clean_df[[x for x in df.columns if x != 'selected' and x != 'resID' and x != 'rating']]
#x.head()

xdum = pd.get_dummies(x, columns=[c for c in x.columns if c != 'selected'])
xdum.head()

import statsmodels.api as sm
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('bmh')

res = sm.OLS(y, xdum, family=sm.families.Binomial()).fit()
res.summary()

df_res = pd.DataFrame({
    'param_name': res.params.keys()
    , 'param_w': res.params.values
    , 'pval': res.pvalues
})
# adding field for absolute of parameters
df_res['abs_param_w'] = np.abs(df_res['param_w'])
# marking field is significant under 95% confidence interval
df_res['is_sig_95'] = (df_res['pval'] < 0.05)
# constructing color naming for each param
df_res['c'] = ['blue' if x else 'red' for x in df_res['is_sig_95']]

# make it sorted by abs of parameter value
df_res = df_res.sort_values(by='abs_param_w', ascending=True)


f, ax = plt.subplots(figsize=(14, 8))
plt.title('Part Worth')
pwu = df_res['param_w']
xbar = np.arange(len(pwu))
plt.barh(xbar, pwu, color=df_res['c'])
#plt.yticks(xbar, labels=df_res['param_name'])
plt.show()



