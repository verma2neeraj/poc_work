# -*- coding: utf-8 -*-
"""
Created on Sun Jul  5 13:43:02 2020

@author: NeerajVerma
"""

import numpy as np
import tensorflow as tf
from tensorflow import keras
import pandas as pd
import seaborn as sns
from pylab import rcParams
import matplotlib.pyplot as plt
from matplotlib import rc

%matplotlib inline
%config InlineBackend.figure_format='retina'


def create_dataset(X, y, time_steps=1):
    Xs, ys = [], []
    for i in range(len(X) - time_steps):
        v = X.iloc[i:(i + time_steps)].values
        Xs.append(v)
        ys.append(y.iloc[i + time_steps])
    return np.array(Xs), np.array(ys)



sns.set(style='whitegrid', palette='muted', font_scale=1.5)
rcParams['figure.figsize'] = 16, 10

df = pd.read_excel("PBI_ORDER_SUMM_COUNT.xlsx")
df.head()
 
df = df [['Date','Value']]

df = df.set_index('Date') 

train_size = int(len(df) * 0.8)
test_size = len(df) - train_size
train, test = df.iloc[0:train_size], df.iloc[train_size:len(df)]
print(len(train), len(test))

time_steps = 5

# reshape to [samples, time_steps, n_features]

X_train, y_train = create_dataset(train, train.Value, time_steps)
X_test, y_test = create_dataset(test, test.Value, time_steps)

#print(X_train.shape, y_train.shape)



model = keras.Sequential()
model.add(keras.layers.LSTM(
  units=128,
  input_shape=(X_train.shape[1], X_train.shape[2])
))
model.add(keras.layers.Dense(units=1))
model.compile(
  loss='mean_squared_error',
  optimizer=keras.optimizers.Adam(0.001)
)

history = model.fit(
    X_train, y_train,
    epochs=10,
    batch_size=7,
    validation_split=0.1,
    verbose=1,
    shuffle=False
)

y_pred = model.predict(X_test)





 