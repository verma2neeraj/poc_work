# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 05:53:21 2020

@author: NeerajVerma
"""
import warnings
import pandas as pd
import numpy as np
#from pandas import datetime
from datetime import date, timedelta

from matplotlib import pyplot as plt
from pandas.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot
from sklearn.metrics import mean_squared_error

def mean_absolute_percentage_error(y_true, y_pred): 
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate an ARIMA model for a given order (p,d,q)
def evaluate_arima_model(X, arima_order):
	# prepare training dataset
	train_size = int(len(X) * 0.66)
	train, test = X[0:train_size], X[train_size:]
	history = [x for x in train]
	# make predictions
	predictions = list()
	for t in range(len(test)):
		model = ARIMA(history, order=arima_order)
		model_fit = model.fit(disp=0)
		yhat = model_fit.forecast()[0]
		predictions.append(yhat)
		history.append(test[t])
	# calculate out of sample error
	error = mean_squared_error(test, predictions)
	return error
 
# evaluate combinations of p, d and q values for an ARIMA model

def evaluate_models(dataset, p_values, d_values, q_values,bestcfg= None):
    dataset = dataset.astype('float32')
    best_score,best_cfg = float("inf"),None
    for p in p_values:
        for d in d_values:
            for q in q_values:
                order = (p,d,q)
                try:
                    mse = evaluate_arima_model(dataset,order)
                    if mse < best_score:
                        best_score,best_cfg = mse,order
                    print('ARIMA%s MSE= %.3f' %(order,mse))
                except:
                    continue
    print('Best ARIMA%s MSE=%.3f' % (best_cfg,best_score))
    return best_cfg
                        
                    



pweek = 17
nweek = pweek + 5

#Order_summ_File = pd.ExcelFile('Order_Summary_29_Jul.xlsx')
Order_summ_File = pd.ExcelFile('Order_summ_2_Aug_20.xlsx')
series_all = pd.read_excel(Order_summ_File, 'Export Worksheet',squeeze=True)

df_cust_forcast = None
df_MAPE_Final = None
df_Actual_MAPE = None
#Cust_Name = 'LINK UP SERVICES LIMITED'


#df_pivot = pd.pivot_table(series_all ,index = ['Customer_Name'] , values = ['DISTINCT_ORDER_CNT'],aggfunc= {'DISTINCT_ORDER_CNT': [np.sum]} )
#df_pivot = pd.DataFrame (df_pivot.reset_index() )
#series_all['REQ_DATE'].dt.month.max() - 1

#group by for monthly order count as per the previous month 
#series = series_all.loc[(series_all['REQ_DATE'].dt.month == series_all['REQ_DATE'].dt.month.max() - 1) & (series_all['REQ_DATE'].dt.year == 2020)]
series = series_all.loc[(series_all['Month-Year'] == '6-2020')]
df_pivot  = series.groupby(['Customer_Name'])['DISTINCT_ORDER_CNT'].aggregate('sum')
# group by for all the order count 
#df_pivot  = series_all.groupby(['Customer_Name'])['DISTINCT_ORDER_CNT'].aggregate('sum')
df_pivot = pd.DataFrame (df_pivot.reset_index() )
df_pivot = df_pivot.sort_values(by=['DISTINCT_ORDER_CNT'], ascending = False)
    
df_pivot = df_pivot.head(20) 
#df_pivot.loc[df_pivot['Customer_Name'] == Cust_Name]
ct = 'LINK UP SERVICES LIMITED'
ct = 'CAPITAL AIRTIME LIMITED'
for ct in df_pivot['Customer_Name']:
    print(ct)
    series_CN = series_all.loc[series_all['Customer_Name'] == ct ]
    series_CN =series_CN.sort_values(by=['REQ_DATE'])
    
    series_CN.rename(columns={'REQ_DATE' :'DATE'},inplace = True)  
    
    
    series_CN.rename(columns={'DISTINCT_ORDER_CNT' :'ORDER_CNT'},inplace = True)  
    series_CN = series_CN [['DATE', 'ORDER_CNT']]
    
    series_CN['ORDER_CNT'] = series_CN['ORDER_CNT'].astype(np.float64)
    series_CN = series_CN.set_index('DATE') 
    
#    series_CN .plot()
#    pyplot.show()
#    
#    autocorrelation_plot(series_CN )
#    pyplot.show()
#    
    
    series_CN_wk_a = series_CN['ORDER_CNT'].resample('W').sum()
    series_CN_wk = series_CN_wk_a[:-pweek]
    #
#    import statsmodels.api as sm
#    decomposition = sm.tsa.seasonal_decompose(series_CN_wk, model='additive')
##    decomposition = sm.tsa.seasonal_decompose(series_CN_wk, model='multiplicative')
#    decomposition.plot()
#    
    
    X =   series_CN_wk.values

    p_values = [2,4,5,6,8]
    d_values = range(0, 3)
    q_values = range(0, 3)
    warnings.filterwarnings("ignore")
    best_order = evaluate_models(X, p_values, d_values, q_values)
    
    
    
    size = int(len(X) * 0.66)
    train, test = X[0:size], X[size:len(X)]
    history = [x for x in train]
    predictions = list()
    for t in range(len(test)):
    #   print(t)
    	model = ARIMA(history, order=best_order)
    	model_fit = model.fit(disp=0)
    	output = model_fit.forecast()
    	yhat = output[0]
    	predictions.append(yhat)
    	obs = test[t]
    	history.append(obs)
    	print('predicted=%f, expected=%f' % (yhat, obs))
        
    MAPE = mean_absolute_percentage_error(test, predictions)
    
    print(ct , '  MAPE :  %.3f' % MAPE)
    
    data = {'Cust_Name': [ct], 'MAPE_Val': [MAPE],'Best_Order' : [best_order] }
    
    df_MAPE = pd.DataFrame( data)
    
    if isinstance(df_MAPE_Final, type(None)):
        df_MAPE_Final = df_MAPE
        print ('first time')
    else:
        df_MAPE_Final = df_MAPE_Final.append(df_MAPE) 
#    pyplot.plot(test)
#    pyplot.plot(predictions, color='red')
#    pyplot.show()
#    
    forecast, stderr, conf = model_fit.forecast(steps=nweek)
    
    dfl = pd.DataFrame(conf)
    #dfl[0]
    #series5.columns
    series_wk = pd.DataFrame(series_CN_wk.reset_index())
    #series.columns
    maxdate = series_wk['DATE'].max()
    
    startdate = maxdate + timedelta(days=7)
    enddate = startdate + timedelta(days=7*nweek-7)
    
    dayseries = pd.date_range(startdate,enddate,freq = 'W')
    
    
    df = pd.DataFrame( {'DATE': dayseries,'Forecast_Order_CNT': forecast , 'lowerlimit':dfl[0] , 'upperlimit': dfl[1], 'Cust_Name' : ct })
    #series_wk.tail()
    #df.head()
    
    series_CN_wk_a = pd.DataFrame(series_CN_wk_a.reset_index())
    df_actual = series_CN_wk_a.tail(pweek)
    
    df = pd.merge(df,df_actual,on  = ['DATE'],how = 'left')
    
    series_wk['Cust_Name'] = ct
    
    #df_final = pd.merge(series_wk,df,  on= ['DATE'],how = 'left')
    df_final = series_wk.append(df)
    
    df_final = df_final [['DATE', 'Cust_Name',  'ORDER_CNT', 'Forecast_Order_CNT','lowerlimit', 'upperlimit']]
    
    if isinstance(df_cust_forcast, type(None)):
        df_cust_forcast = df_final
        print ('yes')
    else:
        df_cust_forcast = df_cust_forcast.append(df_final) 
#    df_cust_forcast  = pd.merge(df_cust_forcast,df_final,on  = ['DATE'])
    
    df_actual = df_actual.set_index('DATE') 
    forecast1 = forecast[0:pweek]
    
#    df_actual = pd.DataFrame(df_actual.reset_index())
#    df_actual1 = df_actual.loc[df_actual['DATE'].isin(dayseries),'ORDER_CNT']
    
    MAPEfinal = mean_absolute_percentage_error(df_actual, forecast1)
    print('Final  MAPE : %.3f' % MAPEfinal)
    
    data_MAPE = {'Cust_Name': [ct], 'Final_MAPE': [MAPEfinal], 'Best_Order' : [best_order] }
    
    df_Final_MAPE = pd.DataFrame( data_MAPE)
    
    if isinstance(df_Actual_MAPE, type(None)):
        df_Actual_MAPE = df_Final_MAPE
        print ('first time')
    else:
        df_Actual_MAPE = df_Actual_MAPE.append(df_Final_MAPE)

    
#SAGE TELECOMMUNICATION LIMITED

df_cust_forcast.to_excel('Order_CNT_Forecast_v2.xlsx' ,index = False)
df_MAPE_Final.to_excel('Order_MAPE_Result_v2.xlsx' ,index = False)
df_Actual_MAPE.to_excel('Order_Final_MAPE_Result_v2.xlsx' ,index = False)
#
#df_final.to_excel('Order_CNT_Forecast.xlsx' ,index = False)