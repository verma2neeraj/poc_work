# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 12:25:14 2020

@author: NeerajVerma
"""

import pandas as pd
from datetime import date, timedelta
import numpy as np

#week_startdate(2020,24)
#week_Enddate(2020,24)
def week_startdate(year, week):
    d = date(year,1,1)
    d = d - timedelta(d.weekday())
    dlt = timedelta(days = (week-1)*7)
    return d + dlt 


#def week_Enddate(year, week):
#    d = date(year,1,1)
#    d = d - timedelta(d.weekday())
#    dlt = timedelta(days = (week-1)*7)
#    return d + dlt + timedelta(days=6)


df_tran = pd.read_excel('Sunrise_Transfer.xlsx')
colname = 'Airtime'

df_tran = df_tran.loc[df_tran['TransactionType'] == colname]


df_tran.columns
df_tran = df_tran[['Date','Value']]
df_tran.rename(columns={'Value':colname}, inplace=True) 
#df_tran  = df.loc[df['TransactionType'] == 'Transfer'] 
df_tran = df_tran.sort_values(by=['Date']) 
df_tran.head()
df_tran.to_excel(colname+ '.xlsx',index = False)
#df_tran.to_excel('Transfer.xlsx',index = False)

df_tran['WeekNo'] = df_tran['Date'].dt.strftime('%V')
df_tran['WeekNo'] = df_tran['WeekNo'].astype(np.int)
df_tran['Year'] = df_tran['Date'].dt.strftime('%Y')
df_tran['Year'] = df_tran['Year'].astype(np.int)


df_tran['Star_Date'] = df_tran.apply(lambda x:week_startdate(x.Year,x.WeekNo),axis=1)    
#df_tran['End_Date'] = df_tran.apply(lambda x:week_Enddate(x.Year,x.WeekNo),axis=1)    

df_tran.head()

df_tran_weekly = df_tran[['Star_Date' , colname]]
#df_tran_weekly.to_excel('Weekly_Airtime.xlsx',index = False)
#df_tran_weekly.to_excel('weekly_Transfer.xlsx',index = False)

df_tran_weekly = pd.pivot_table(df_tran, index = ['Star_Date'] ,values = colname,aggfunc= {colname: [np.sum]} )
df_tran_weekly  = pd.DataFrame(df_tran_weekly.reset_index())
df_tran_weekly['Star_Date'] = pd.to_datetime(df_tran_weekly['Star_Date'])
df_tran_weekly = df_tran_weekly.sort_values(by=['Star_Date']) 

df_tran_weekly.to_excel('Weekly_'+colname+ '.xlsx',index = False)


#TPS data 

df_TPS = pd.read_excel('CPUTPSData_Masterfile.xlsx')

df_TPS.columns
df_TPS = df_TPS[['DATE','AVG_TPS']]
#df_TPS.rename(columns={'Value':'Transfer'}, inplace=True) 
#df_TPS  = df.loc[df['TransactionType'] == 'Transfer'] 
df_TPS = df_TPS.sort_values(by=['Date']) 
df_TPS.head()
df_TPS.to_excel('Daily_Avg_TPS.xlsx',index = False)

df_TPS['WeekNo'] = df_TPS['DATE'].dt.strftime('%V')
df_TPS['WeekNo'] = df_TPS['WeekNo'].astype(np.int)
df_TPS['Year'] = df_TPS['DATE'].dt.strftime('%Y')
df_TPS['Year'] = df_TPS['Year'].astype(np.int)


df_TPS['Star_Date'] = df_TPS.apply(lambda x:week_startdate(x.Year,x.WeekNo),axis=1)    
#df_TPS['End_Date'] = df_TPS.apply(lambda x:week_Enddate(x.Year,x.WeekNo),axis=1)    

df_TPS.head()

df_TPS_weekly = df_TPS[['Star_Date' , 'AVG_TPS']]
#df_TPS_weekly.to_excel('weekly_Transfer.xlsx',index = False)

df_TPS_weekly = pd.pivot_table(df_TPS, index = ['Star_Date'] ,values = 'AVG_TPS',aggfunc= {'AVG_TPS': [np.sum]} )
df_TPS_weekly  = pd.DataFrame(df_TPS_weekly.reset_index())
df_TPS_weekly['Star_Date'] = pd.to_datetime(df_TPS_weekly['Star_Date'])
df_TPS_weekly = df_TPS_weekly.sort_values(by=['Star_Date']) 

df_TPS_weekly.to_excel('Weekly_TPS.xlsx',index = False)
